import Vue from 'vue'
import VueResource from 'vue-resource'
import Interceptors from'./interceptors'

Vue.use(VueResource)
Vue.http.interceptors.push(Interceptors);

let base = 'http://localhost:9000';
//let base2 = 'https://hornbill-api-service.p-m7pmn-pipeline.svc.cluster.local:8000/api'
//let base2 = 'http://localhost:8000/api';
let base2 ='https://api.hornbill.io/api'

//let header = sessionStorage.getItem('id_token') !== null ? sessionStorage.getItem('id_token') : '';


export const loginRequest = params => { return Vue.http.post(`${base}/auth`, params); };

export const registerRequest = params => { return Vue.http.post(`${base}/auth/register`, params); };

export const activationRequest = params => { return Vue.http.post(`${base}/auth/activate`, params); };

export const requestUserInfo = params => { return Vue.http.post(`${base}/auth/user/` + params); };


export const pdfRequest = params => { return Vue.http.post(`${base}/invoicePdf`, params); };

export const deleteRequest = params => { return Vue.http.post(`${base}/invoicePdf`, params); };

//export const createPdfRequest = params => { return Vue.http.get(`${base2}/createPdf`, params); };

// ----------------------------------------------------------------------------------------------------
// BASEURL: https://api.hornbill.io/api
// -----------------------------------------------------------------------------------------------------

export const createPdfRequest = params => { return Vue.http.post(`${base2}/invoices`, params); };

export const deletePdfRequest = params => { return Vue.http.post(`${base2}/deleteInvoice`, params); };


