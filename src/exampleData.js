export default {
     invoice2 : {
        companyDetails: {
            name: 'Smurff Inc',
            addressLine1: 'Halkoniemenkatu 5D 113',
            addressLine2: '',
            zipcode: '33410',
            city:'Tampere',
            stateOrCountry: 'Finland',
            email: 'smurff@smurff.io',
            mobile: '75930198',
        },
        recipientDetails: {
            fullName: 'Vaxly Inc',
            addressLine1: 'Banerinkatu 7I 33',
            addressLine2: '',
            zipcode: '57800',
            city:'Tampere',
            stateOrCountry: 'Finland',
            email: 'vaxly@vaxly.io',
            mobile: '784028795',
        },
        invoiceDetails: {
            logo: this.$store.state.invoice.invoiceDetails.logo,
            invoiceNo: 'INV-0001',
            invoiceDate: new Date(moment()),
            invoiceDateVal: moment().format('ddd Do MMM YYYY'),
            dueDate: new Date(moment().add(2, 'weeks')),
            dueDateVal: moment().add(2, 'weeks').format('ddd Do MMM YYYY'),
        },
        invoiceData: {
            items: [
            {
                description: "Website Design",
                price: 40,
                quantity: 30,
                amount: 1200
            },
            {
                description: "Website Development",
                price: 50,
                quantity: 60,
                amount: 3000
            },
            {
                description: "Website Optimization",
                price: 50,
                quantity: 30,
                amount: 1500
            }
            ],
            currency: '\u20ac',
            currencyValue: "&euro;",
            subTotal: 5700.00,
            discount: null,
            discountPercentage: 0,
            discountedAmount: 0,
            taxIncl: true,
            taxPercent: 15,
            tax: 855.00,
            total: 5700.00,
        },
    }
}