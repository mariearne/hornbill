/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
==========================================================================================*/


export default [
  {
    url: '/',
    name: "Invoice",
    icon: "BookOpenIcon",
    slug: "invoice",
    i18n: "Invoice",
  },
  {
    url: 'https://receipt.hornbill.io',
    name: "Receipt",
    icon: "CopyIcon",
    slug: "external",
    i18n: "Receipt",
  },
  {
    url: 'https://estimate.hornbill.io',
    name: "Estimate",
    icon: "MaximizeIcon",
    slug: "external",
    i18n: "Estimate",
  },
  // {
  //   url: "/home",
  //   name: "Home",
  //   slug: "home",
  //   icon: "HomeIcon",
  // },
  // {
  //   url: "/page2",
  //   name: "Page 2",
  //   slug: "page2",
  //   icon: "FileIcon",
  // },
  // {
  //   url: null,
  //   name: "Dashboard",
  //   tag: "2",
  //   tagColor: "warning",
  //   icon: "HomeIcon",
  //   i18n: "Dashboard",
  //   submenu: [
  //     {
  //       url: "/dashboard/analytics",
  //       name: "Analytics",
  //       slug: "dashboard-analytics",
  //       i18n: "Analytics"
  //     },
  //     {
  //       url: "/dashboard/ecommerce",
  //       name: "eCommerce",
  //       slug: "dashboard-ecommerce",
  //       i18n: "eCommerce"
  //     }
  //   ]
  // },
  // {
  //   url: null,
  //   name: "Support",
  //   icon: "SmileIcon",
  //   i18n: "Support",
  //   submenu: [
  //     {
  //       url: 'https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/documentation/',
  //       name: "Documentation",
  //       icon: "BookOpenIcon",
  //       slug: "external",
  //       i18n: "Documentation",
  //       target: "_blank"
  //     },
  //     {
  //       url: 'https://pixinvent.ticksy.com/',
  //       name: "Raise Support",
  //       icon: "LifeBuoyIcon",
  //       slug: "external",
  //       i18n: "RaiseSupport",
  //       target: "_blank"
  //     },
  //   ]
  // },
]
