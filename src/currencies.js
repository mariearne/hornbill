export default {
    currencies: [
          {
            "cc": "EUR",
            "label": "\u20ac",
            "value": "&euro;"
          },
          {
            "cc": "USD",
            "label": "US$",
            "value": "US$"
          },
          {
            "cc": "GBP",
            "label": "\u00a3 ",
            "value": "\u00a3 "
          },
          {
            "cc": "KES",
            "label": "KSh",
            "value": "KSh"
          },
          {
            "cc": "AED",
            "label": "\u062f.\u0625;",
            "value": "\u062f.\u0625;"
          },
          {
            "cc": "AFN",
            "label": "Afs",
            "value": "Afs"
          },
          {
            "cc": "ALL",
            "label": "L",
            "value": "L"
          },
          {
            "cc": "AMD",
            "label": "AMD",
            "value": "AMD"
          },
          {
            "cc": "ANG",
            "label": "NA\u0192",
            "value": "NA\u0192"
          },
          {
            "cc": "AOA",
            "label": "Kz",
            "value": "Kz"
          },
          {
            "cc": "ARS",
            "label": "$",
            "value": "$"
          },
          {
            "cc": "AUD",
            "label": "$",
            "value": "$"
          },
          {
            "cc": "AWG",
            "label": "\u0192",
            "value": "\u0192"
          },
          {
            "cc": "AZN",
            "label": "AZN",
            "value": "AZN"
          },
          {
            "cc": "BAM",
            "label": "KM",
            "value": "KM"
          },
          {
            "cc": "BBD",
            "label": "Bds$",
            "value": "Bds$"
          },
          {
            "cc": "BDT",
            "label": "\u09f3",
            "value": "\u09f3"
          },
          {
            "cc": "BGN",
            "label": "BGN",
            "value": "BGN"
          },
          {
            "cc": "BHD",
            "label": ".\u062f.\u0628",
            "value": ".\u062f.\u0628"
          },
          {
            "cc": "BIF",
            "label": "FBu",
            "value": "FBu"
          },
          {
            "cc": "BMD",
            "label": "BD$",
            "value": "BD$"
          },
          {
            "cc": "BND",
            "label": "B$",
            "value": "B$"
          },
          {
            "cc": "BOB",
            "label": "Bs.",
            "value": "Bs."
          },
          {
            "cc": "BRL",
            "label": "R$",
            "value": "R$"
          },
          {
            "cc": "BSD",
            "label": "B$",
            "value": "B$"
          },
          {
            "cc": "BTN",
            "label": "Nu.",
            "value": "Nu."
          },
          {
            "cc": "BWP",
            "label": "P",
            "value": "P"
          },
          {
            "cc": "BYR",
            "label": "Br",
            "value": "Br"
          },
          {
            "cc": "BZD",
            "label": "BZ$",
            "value": "BZ$"
          },
          {
            "cc": "CAD",
            "label": "$",
            "value": "$"
          },
          {
            "cc": "CDF",
            "label": "F",
            "value": "F"
          },
          {
            "cc": "CHF",
            "label": "Fr.",
            "value": "Fr."
          },
          {
            "cc": "CLP",
            "label": "$",
            "value": "$"
          },
          {
            "cc": "CNY",
            "label": "\u00a5",
            "value": "\u00a5"
          },
          {
            "cc": "COP",
            "label": "Col$",
            "value": "Col$"
          },
          {
            "cc": "CRC",
            "label": "\u20a1",
            "value": "\u20a1"
          },
          {
            "cc": "CUC",
            "label": "$",
            "value": "$"
          },
          {
            "cc": "CVE",
            "label": "Esc",
            "value": "Esc"
          },
          {
            "cc": "CZK",
            "label": "K\u010d",
            "value": "K\u010d"
          },
          {
            "cc": "DJF",
            "label": "Fdj",
            "value": "Fdj"
          },
          {
            "cc": "DKK",
            "label": "Kr",
            "value": "Kr"
          },
          {
            "cc": "DOP",
            "label": "RD$",
            "value": "RD$"
          },
          {
            "cc": "DZD",
            "label": "\u062f.\u062c",
            "value": "\u062f.\u062c"
          },
          {
            "cc": "EEK",
            "label": "KR",
            "value": "KR"
          },
          {
            "cc": "EGP",
            "label": "\u00a3",
            "value": "\u00a3"
          },
          {
            "cc": "ERN",
            "label": "Nfa",
            "value": "Nfa"
          },
          {
            "cc": "ETB",
            "label": "Br",
            "value": "Br"
          },
          {
            "cc": "FJD",
            "label": "FJ$",
            "value": "FJ$"
          },
          {
            "cc": "FKP",
            "label": "\u00a3",
            "value": "\u00a3"
          },
          {
            "cc": "GEL",
            "label": "GEL",
            "value": "GEL"
          },
          {
            "cc": "GHS",
            "label": "GH\u20b5",
            "value": "GH\u20b5"
          },
          {
            "cc": "GIP",
            "label": "\u00a3",
            "value": "\u00a3"
          },
          {
            "cc": "GMD",
            "label": "D",
            "value": "D"
          },
          {
            "cc": "GNF",
            "label": "FG",
            "value": "FG"
          },
          {
            "cc": "GQE",
            "label": "CFA",
            "value": "CFA"
          },
          {
            "cc": "GTQ",
            "label": "Q",
            "value": "Q"
          },
          {
            "cc": "GYD",
            "label": "GY$",
            "value": "GY$"
          },
          {
            "cc": "HKD",
            "label": "HK$",
            "value": "HK$"
          },
          {
            "cc": "HNL",
            "label": "L",
            "value": "L"
          },
          {
            "cc": "HRK",
            "label": "kn",
            "value": "kn"
          },
          {
            "cc": "HTG",
            "label": "G",
            "value": "G"
          },
          {
            "cc": "HUF",
            "label": "Ft",
            "value": "Ft"
          },
          {
            "cc": "IDR",
            "label": "Rp",
            "value": "Rp"
          },
          {
            "cc": "ILS",
            "label": "\u20aa",
            "value": "\u20aa"
          },
          {
            "cc": "INR",
            "label": "\u20B9",
            "value": "\u20B9"
          },
          {
            "cc": "IQD",
            "label": "\u062f.\u0639",
            "value": "\u062f.\u0639"
          },
          {
            "cc": "IRR",
            "label": "IRR",
            "value": "IRR"
          },
          {
            "cc": "ISK",
            "label": "kr",
            "value": "kr"
          },
          {
            "cc": "JMD",
            "label": "J$",
            "value": "J$"
          },
          {
            "cc": "JOD",
            "label": "JOD",
            "value": "JOD"
          },
          {
            "cc": "JPY",
            "label": "\u00a5",
            "value": "\u00a5"
          },
          {
            "cc": "KGS",
            "label": "\u0441\u043e\u043c",
            "value": "\u0441\u043e\u043c"
          },
          {
            "cc": "KHR",
            "label": "\u17db",
            "value": "\u17db"
          },
          {
            "cc": "KMF",
            "label": "KMF",
            "value": "KMF"
          },
          {
            "cc": "KPW",
            "label": "W",
            "value": "W"
          },
          {
            "cc": "KRW",
            "label": "W",
            "value": "W"
          },
          {
            "cc": "KWD",
            "label": "KWD",
            "value": "KWD"
          },
          {
            "cc": "KYD",
            "label": "KY$",
            "value": "KY$"
          },
          {
            "cc": "KZT",
            "label": "T",
            "value": "T"
          },
          {
            "cc": "LAK",
            "label": "KN",
            "value": "KN"
          },
          {
            "cc": "LBP",
            "label": "\u00a3",
            "value": "\u00a3"
          },
          {
            "cc": "LKR",
            "label": "Rs",
            "value": "Rs"
          },
          {
            "cc": "LRD",
            "label": "L$",
            "value": "L$"
          },
          {
            "cc": "LSL",
            "label": "M",
            "value": "M"
          },
          {
            "cc": "LTL",
            "label": "Lt",
            "value": "Lt"
          },
          {
            "cc": "LVL",
            "label": "Ls",
            "value": "Ls"
          },
          {
            "cc": "LYD",
            "label": "LD",
            "value": "LD"
          },
          {
            "cc": "MAD",
            "label": "MAD",
            "value": "MAD"
          },
          {
            "cc": "MDL",
            "label": "MDL",
            "value": "MDL"
          },
          {
            "cc": "MGA",
            "label": "FMG",
            "value": "FMG"
          },
          {
            "cc": "MKD",
            "label": "MKD",
            "value": "MKD"
          },
          {
            "cc": "MMK",
            "label": "K",
            "value": "K"
          },
          {
            "cc": "MNT",
            "label": "\u20ae",
            "value": "\u20ae"
          },
          {
            "cc": "MOP",
            "label": "P",
            "value": "P"
          },
          {
            "cc": "MRO",
            "label": "UM",
            "value": "UM"
          },
          {
            "cc": "MUR",
            "label": "Rs",
            "value": "Rs"
          },
          {
            "cc": "MVR",
            "label": "Rf",
            "value": "Rf"
          },
          {
            "cc": "MWK",
            "label": "MK",
            "value": "MK"
          },
          {
            "cc": "MXN",
            "label": "$",
            "value": "$"
          },
          {
            "cc": "MYR",
            "label": "RM",
            "value": "RM"
          },
          {
            "cc": "MZM",
            "label": "MTn",
            "value": "MTn"
          },
          {
            "cc": "NAD",
            "label": "N$",
            "value": "N$"
          },
          {
            "cc": "NGN",
            "label": "\u20a6",
            "value": "\u20a6"
          },
          {
            "cc": "NIO",
            "label": "C$",
            "value": "C$"
          },
          {
            "cc": "NOK",
            "label": "kr",
            "value": "kr"
          },
          {
            "cc": "NPR",
            "label": "NRs",
            "value": "NRs"
          },
          {
            "cc": "NZD",
            "label": "NZ$",
            "value": "NZ$"
          },
          {
            "cc": "OMR",
            "label": "OMR",
            "value": "OMR"
          },
          {
            "cc": "PAB",
            "label": "B./",
            "value": "B./"
          },
          {
            "cc": "PEN",
            "label": "S/.",
            "value": "S/."
          },
          {
            "cc": "PGK",
            "label": "K",
            "value": "K"
          },
          {
            "cc": "PHP",
            "label": "\u20b1",
            "value": "\u20b1"
          },
          {
            "cc": "PKR",
            "label": "Rs.",
            "value": "Rs."
          },
          {
            "cc": "PLN",
            "label": "z\u0142",
            "value": "z\u0142"
          },
          {
            "cc": "PYG",
            "label": "\u20b2",
            "value": "\u20b2"
          },
          {
            "cc": "QAR",
            "label": "QR",
            "value": "QR"
          },
          {
            "cc": "RON",
            "label": "L",
            "value": "L"
          },
          {
            "cc": "RSD",
            "label": "din.",
            "value": "din."
          },
          {
            "cc": "RUB",
            "label": "R",
            "value": "R"
          },
          {
            "cc": "SAR",
            "label": "SR",
            "value": "SR"
          },
          {
            "cc": "SBD",
            "label": "SI$",
            "value": "SI$"
          },
          {
            "cc": "SCR",
            "label": "SR",
            "value": "SR"
          },
          {
            "cc": "SDG",
            "label": "SDG",
            "value": "SDG"
          },
          {
            "cc": "SEK",
            "label": "kr",
            "value": "kr"
          },
          {
            "cc": "SGD",
            "label": "S$",
            "value": "S$"
          },
          {
            "cc": "SHP",
            "label": "\u00a3",
            "value": "\u00a3"
          },
          {
            "cc": "SLL",
            "label": "Le",
            "value": "Le"
          },
          {
            "cc": "SOS",
            "label": "Sh.",
            "value": "Sh."
          },
          {
            "cc": "SRD",
            "label": "$",
            "value": "$"
          },
          {
            "cc": "SYP",
            "label": "LS",
            "value": "LS"
          },
          {
            "cc": "SZL",
            "label": "E",
            "value": "E"
          },
          {
            "cc": "THB",
            "label": "\u0e3f",
            "value": "\u0e3f"
          },
          {
            "cc": "TJS",
            "label": "TJS",
            "value": "TJS"
          },
          {
            "cc": "TMT",
            "label": "m",
            "value": "m"
          },
          {
            "cc": "TND",
            "label": "DT",
            "value": "DT"
          },
          {
            "cc": "TRY",
            "label": "TRY",
            "value": "TRY"
          },
          {
            "cc": "TTD",
            "label": "TT$",
            "value": "TT$"
          },
          {
            "cc": "TWD",
            "label": "NT$",
            "value": "NT$"
          },
          {
            "cc": "TZS",
            "label": "TZS",
            "value": "TZS"
          },
          {
            "cc": "UAH",
            "label": "UAH",
            "value": "UAH"
          },
          {
            "cc": "UGX",
            "label": "USh",
            "value": "USh"
          },
          {
            "cc": "UYU",
            "label": "$U",
            "value": "$U"
          },
          {
            "cc": "UZS",
            "label": "UZS",
            "value": "UZS"
          },
          {
            "cc": "VEB",
            "label": "Bs",
            "value": "Bs"
          },
          {
            "cc": "VND",
            "label": "\u20ab",
            "value": "\u20ab"
          },
          {
            "cc": "VUV",
            "label": "VT",
            "value": "VT"
          },
          {
            "cc": "WST",
            "label": "WS$",
            "value": "WS$"
          },
          {
            "cc": "XAF",
            "label": "CFA",
            "value": "CFA"
          },
          {
            "cc": "XCD",
            "label": "EC$",
            "value": "EC$"
          },
          {
            "cc": "XDR",
            "label": "SDR",
            "value": "SDR"
          },
          {
            "cc": "XOF",
            "label": "CFA",
            "value": "CFA"
          },
          {
            "cc": "XPF",
            "label": "F",
            "value": "F"
          },
          {
            "cc": "YER",
            "label": "YER",
            "value": "YER"
          },
          {
            "cc": "ZAR",
            "label": "R",
            "value": "R"
          },
          {
            "cc": "ZMK",
            "label": "ZK",
            "value": "ZK"
          },
          {
            "cc": "ZWR",
            "label": "Z$",
            "value": "Z$"
          }
        ]
}