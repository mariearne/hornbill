/*=========================================================================================
  File Name: state.js
  Description: Vuex Store - state
  ----------------------------------------------------------------------------------------

==========================================================================================*/

import navbarSearchAndPinList from "@/layouts/components/navbar/navbarSearchAndPinList"
import themeConfig from "@/../themeConfig.js"
import colors from "@/../themeConfig.js"
var moment = require('moment');


// /////////////////////////////////////////////
// Variables
// /////////////////////////////////////////////

const userDefaults = {
  uid: 0,          // From Auth
  displayName: "John Doe", // From Auth
  about: "Dessert chocolate cake lemon drops jujubes. Biscuit cupcake ice cream bear claw brownie brownie marshmallow.",
  photoURL: require("@/assets/images/portrait/small/avatar-s-11.jpg"), // From Auth
  status: "online",
  userRole: "admin"
}

// /////////////////////////////////////////////
// State
// /////////////////////////////////////////////

const state = {
  AppActiveUser: userDefaults,
  bodyOverlay: false,
  isVerticalNavMenuActive: true,
  mainLayoutType: themeConfig.mainLayoutType || "horizontal",
  navbarSearchAndPinList: navbarSearchAndPinList,
  reduceButton: themeConfig.sidebarCollapsed,
  verticalNavMenuWidth: "default",
  verticalNavMenuItemsMin: false,
  scrollY: 0,
  starredPages: navbarSearchAndPinList["pages"].data.filter((page) => page.is_bookmarked),
  theme: themeConfig.theme || "light",
  themePrimaryColor: colors.primary,

  invoice: {
    companyDetails: {
      name: 'Your Company Name',
      addressLine1: 'Main Street 43',
      addressLine2: '',
      zipcode: '00000',
      city:'City',
      stateOrCountry: 'Country',
      email: 'email@example.com',
      mobile: '+12 345 678910',
    },
    recipientDetails: {
      fullName: 'Recipient Name',
      addressLine1: '44th Street 37',
      addressLine2: '',
      zipcode: '00000',
      city:'City',
      stateOrCountry: 'Country',
      email: 'email@example.com',
      mobile: '+12 345 0123456',
    },
    invoiceDetails: {
      logo: '',
      invoiceNo: '001',
      invoiceDate: new Date(moment()),
      invoiceDateVal: moment().format('ddd Do MMM YYYY'),
      dueDate: new Date(moment().add(2, 'weeks')),
      dueDateVal: moment().add(2, 'weeks').format('ddd Do MMM YYYY'),
    },
    invoiceData: {
      items: [],
      currency: '\u20ac',
      currencyValue: "&euro;",
      subTotal: 0.00,
      discount: null,
      discountPercentage: 0,
      discountedAmount: 0,
      taxIncl: true,
      taxPercent: 15,
      tax: 0.00,
      total: 0.00,
    },
  },
  pdfUrl: '',

    // Can be used to get current window with
    // Note: Above breakpoint state is for internal use of sidebar & navbar component
    windowWidth: null,
}

export default state
