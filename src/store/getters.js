/*=========================================================================================
  File Name: getters.js
  Description: Vuex Store - getters
  ----------------------------------------------------------------------------------------

==========================================================================================*/

// added so later we can keep breakpoint in sync automatically using this config file
// import tailwindConfig from "../../tailwind.config.js"

const getters = {

	// COMPONENT
		// vx-autosuggest
	// starredPages: state => state.navbarSearchAndPinList.data.filter((page) => page.highlightAction),
  windowBreakPoint: state => {

    // This should be same as tailwind. So, it stays in sync with tailwind utility classes
    if (state.windowWidth >= 1200) return "xl"
    else if (state.windowWidth >= 992) return "lg"
    else if (state.windowWidth >= 768) return "md"
    else if (state.windowWidth >= 576) return "sm"
    else return "xs"
  },

  // INVOICE GETTERS
  getLogo(state) {
    return state.invoice.invoiceDetails.logo
  },
  getInvoiceNo(state) {
    return state.invoice.invoiceDetails.invoiceNo
  },
  getInvoiceDate(state) {
    return state.invoice.invoiceDetails.invoiceDate
  },
  getDueDate(state) {
    return state.invoice.invoiceDetails.dueDate
  },
  getCompanyDetails(state) {
    return state.invoice.companyDetails
  },
  getRecipientDetails(state) {
    return state.invoice.recipientDetails
  },
  getItems(state){
    return state.invoice.invoiceData.items
  },
  getSubTotal(state){
    var subTotal = 0;
    state.invoice.invoiceData.items.forEach(item => {
      subTotal += item.amount;
    });
    subTotal = Math.round(subTotal * 100) / 100;
    return subTotal.toFixed(2);
  },
  getTax(state){
    var tax = 0.00;
    tax = Math.round(((state.invoice.invoiceData.taxPercent / 100) * state.invoice.invoiceData.subTotal) * 100) / 100;
    return tax.toFixed(2);
  },
  getDiscountAmount(state){
    var amount = 0.00;
    if(state.invoice.invoiceData.discount === 'percent' && state.invoice.invoiceData.discountPercentage > 0){
      amount = Math.round(((state.invoice.invoiceData.discountPercentage / 100) * state.invoice.invoiceData.subTotal) * 100) / 100;
    }
    else if(state.invoice.invoiceData.discount === 'amount' && state.invoice.invoiceData.discountedAmount > 0){
      amount = Number(state.invoice.invoiceData.discountedAmount);
    }
    return amount.toFixed(2);
  },
  getGrandTotal(state){
    var total;
    if(state.invoice.invoiceData.taxIncl == true){
      total = Math.round((state.invoice.invoiceData.subTotal) * 100) / 100;
    }
    else{
      total = Math.round((Number(state.invoice.invoiceData.tax) + Number(state.invoice.invoiceData.subTotal)) * 100) / 100
    }
    if(state.invoice.invoiceData.discount !== null){
      total = Math.round((total - Number(state.invoice.invoiceData.discountedAmount)) * 100) / 100;
    }
    return total.toFixed(2);
  },

  getInvoiceData(state){
    // const data = {
    //     logo: state.invoice.invoiceDetails.logo,
    //     invoiceNumber: state.invoice.invoiceDetails.invoiceNo,
    //     invoiceDate: state.invoice.invoiceDetails.invoiceDate,
    //     dueDate: state.invoice.invoiceDetails.dueDate,
    //     companyDetails: state.invoice.companyDetails,
    //     recipientDetails: state.invoice.recipientDetails,
    //     items: state.invoice.invoiceData.items,
    //     currency: state.invoice.invoiceData.currency,
    //     subTotal: state.invoice.invoiceData.subTotal,
    //     discountPercentage: state.invoice.invoiceData.discountPercentage,
    //     discountedAmount: state.invoice.invoiceData.discountedAmount,
    //     taxIncl: state.invoice.invoiceData.taxIncl,
    //     taxPercent: state.invoice.invoiceData.taxPercent,
    //     tax: state.invoice.invoiceData.tax,
    //     grandTotal: state.invoice.invoiceData.grandTotal,
    // }
    // return data;
    const data = {
      companyDetails: state.invoice.companyDetails,
      recipientDetails: state.invoice.recipientDetails,
      invoiceDetails: state.invoice.invoiceDetails,
      invoiceData: state.invoice.invoiceData
    }
    return data;
}


}

export default getters
