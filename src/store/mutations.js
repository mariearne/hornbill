/*=========================================================================================
  File Name: mutations.js
  Description: Vuex Store - mutations
  ----------------------------------------------------------------------------------------

==========================================================================================*/
//import Vue from 'vue'

const mutations = {


  // /////////////////////////////////////////////
  // COMPONENTS
  // /////////////////////////////////////////////

  // Vertical NavMenu

  TOGGLE_IS_VERTICAL_NAV_MENU_ACTIVE(state, value) {
    state.isVerticalNavMenuActive = value
  },
  TOGGLE_REDUCE_BUTTON(state, val) {
    state.reduceButton = val
  },
  UPDATE_MAIN_LAYOUT_TYPE(state, val) {
    state.mainLayoutType = val
  },
  UPDATE_VERTICAL_NAV_MENU_ITEMS_MIN(state, val) {
    state.verticalNavMenuItemsMin = val
  },
  UPDATE_VERTICAL_NAV_MENU_WIDTH(state, width) {
    state.verticalNavMenuWidth = width
  },


  // VxAutoSuggest

  UPDATE_STARRED_PAGE(state, payload) {

    // find item index in search list state
    const index = state.navbarSearchAndPinList["pages"].data.findIndex((item) => item.url == payload.url)

    // update the main list
    state.navbarSearchAndPinList["pages"].data[index].is_bookmarked = payload.val

    // if val is true add it to starred else remove
    if (payload.val) {
      state.starredPages.push(state.navbarSearchAndPinList["pages"].data[index])
    }
    else {
      // find item index from starred pages
      const index = state.starredPages.findIndex((item) => item.url == payload.url)

      // remove item using index
      state.starredPages.splice(index, 1)
    }
  },

  // Navbar-Vertical

  ARRANGE_STARRED_PAGES_LIMITED(state, list) {
    const starredPagesMore = state.starredPages.slice(10)
    state.starredPages     = list.concat(starredPagesMore)
  },
  ARRANGE_STARRED_PAGES_MORE(state, list) {
    let downToUp                 = false
    let lastItemInStarredLimited = state.starredPages[10]
    const starredPagesLimited    = state.starredPages.slice(0, 10)
    state.starredPages           = starredPagesLimited.concat(list)

    state.starredPages.slice(0, 10).map((i) => {
      if (list.indexOf(i) > -1) downToUp = true
    })

    if (!downToUp) {
      state.starredPages.splice(10, 0, lastItemInStarredLimited)
    }
  },


  // ////////////////////////////////////////////
  // UI
  // ////////////////////////////////////////////

  TOGGLE_CONTENT_OVERLAY(state, val) { state.bodyOverlay       = val },
  UPDATE_PRIMARY_COLOR(state, val)   { state.themePrimaryColor = val },
  UPDATE_THEME(state, val)           { state.theme             = val },
  UPDATE_WINDOW_WIDTH(state, width)  { state.windowWidth       = width },
  UPDATE_WINDOW_SCROLL_Y(state, val) { state.scrollY = val },


  // /////////////////////////////////////////////
  // User/Account
  // /////////////////////////////////////////////

  // Updates user info in state and localstorage
  UPDATE_USER_INFO(state, payload) {

    // Get Data localStorage
    let userInfo = JSON.parse(localStorage.getItem("userInfo")) || state.AppActiveUser

    for (const property of Object.keys(payload)) {

      if (payload[property] != null) {
        // If some of user property is null - user default property defined in state.AppActiveUser
        state.AppActiveUser[property] = payload[property]

        // Update key in localStorage
        userInfo[property] = payload[property]
      }


    }
    // Store data in localStorage
    localStorage.setItem("userInfo", JSON.stringify(userInfo))
  },

  setPdfUrl(state, data) {
    state.pdfUrl= data
  },

  // INVOICE MUTATIONS

  setLogo(state, data) {
    state.invoice.invoiceDetails.logo = data
  },
  setInvoiceNo(state, data) {
    state.invoice.invoiceDetails.invoiceNo = data
  },
  setInvoiceDate(state, data) {
    state.invoice.invoiceDetails.invoiceDate = data
  },
  setinvoiceDateVal(state, data) {
    state.invoice.invoiceDetails.invoiceDateVal = data
  },
  setDueDate(state, data) {
    state.invoice.invoiceDetails.dueDate = data
  },
  setDueDateVal(state, data) {
    state.invoice.invoiceDetails.dueDateVal = data
  },
  // ----------------------------------------------------------------
  setCompanyDetails(state, data) {
    state.invoice.companyDetails = data;
  },
  setCompanyName(state, data){
    state.invoice.companyDetails.name = data;
  },
  setCompanyAddressLine1(state, data){
    state.invoice.companyDetails.addressLine1 = data;
  },
  setCompanyAddressLine2(state, data){
    state.invoice.companyDetails.addressLine2 = data;
  },
  setCompanyZipcode(state, data){
    state.invoice.companyDetails.zipcode = data;
  },
  setCompanyCity(state, data){
    state.invoice.companyDetails.city = data;
  },
  setCompanyStateOrCountry(state, data){
    state.invoice.companyDetails.stateOrCountry = data;
  },
  setCompanyEmail(state, data){
    state.invoice.companyDetails.email = data;
  },
  setCompanyMobile(state, data){
    state.invoice.companyDetails.mobile = data;
  },
  //-------------------------------------------------------------------
  setRecipientDetails(state, data) {
    state.invoice.recipientDetails = data
  },
  setRecipientFullName(state, data) {
    state.invoice.recipientDetails.fullName = data
  },
  setRecipientAddressLine1(state, data){
    state.invoice.recipientDetails.addressLine1 = data;
  },
  setRecipientAddressLine2(state, data){
    state.invoice.recipientDetails.addressLine2 = data;
  },
  setRecipientZipcode(state, data){
    state.invoice.recipientDetails.zipcode = data;
  },
  setRecipientCity(state, data){
    state.invoice.recipientDetails.city = data;
  },
  setRecipientStateOrCountry(state, data){
    state.invoice.recipientDetails.stateOrCountry = data;
  },
  setRecipientEmail(state, data){
    state.invoice.recipientDetails.email = data;
  },
  setRecipientMobile(state, data){
    state.invoice.recipientDetails.mobile = data;
  },
  //-------------------------------------------------------------------
  
  setItems(state, data) {
    state.invoice.invoiceData.items = data
  },
  addItem(state, data){
    state.invoice.invoiceData.items.push(data);
  },
  delItem(state, data){
    var index = data;
    state.invoice.invoiceData.items.splice(index, 1);
  },
  updateItems(state, {index, field, value}){
    Object.assign(state.invoice.invoiceData.items[index], {
      [field]: value
    });
    var amount = Math.round((state.invoice.invoiceData.items[index].quantity * state.invoice.invoiceData.items[index].price) * 100) / 100;
    Object.assign(state.invoice.invoiceData.items[index], {
      ['amount']: amount
    });
  },
  setCurrency(state, data) {
    state.invoice.invoiceData.currency = data
  },
  setCurrencyValue(state, data) {
    state.invoice.invoiceData.currencyValue = data
  },
  setSubTotal(state, data) {
    state.invoice.invoiceData.subTotal = data;
  },
  setDiscount(state, data){
    state.invoice.invoiceData.discount = data;
  },
  setDiscountPercentage(state, data) {
    state.invoice.invoiceData.discountPercentage = data
  },
  setDiscountedAmount(state, data) {
    state.invoice.invoiceData.discountedAmount = data
  },
  setTaxIncl(state, data) {
    state.invoice.invoiceData.taxIncl = data
  },
  setTaxPercent(state, data) {
    state.invoice.invoiceData.taxPercent = data
  },
  setTax(state, data) {
    state.invoice.invoiceData.tax = data
  },
  setGrandTotal(state, data) {
    state.invoice.invoiceData.total = data
  },

  setInvoiceData(state, data){
    // state.invoice.invoiceDetails.logo             = data.invoiceDetails.logo;
    // state.invoice.invoiceDetails.invoiceNo        = data.invoiceDetails.invoiceNo;
    // state.invoice.invoiceDetails.invoiceDate      = data.invoiceDetails.invoiceDate;
    // state.invoice.invoiceDetails.dueDate          = data.invoiceDetails.dueDate;
    // state.invoice.companyDetails                  = data.companyDetails;
    // state.invoice.recipientDetails                = data.recipientDetails;
    // state.invoice.invoiceData.items               = data.invoiceData.items;
    // state.invoice.invoiceData.currency            = data.invoiceData.currency;
    // state.invoice.invoiceData.subTotal            = data.invoiceData.subTotal;
    // state.invoice.invoiceData.discountPercentage  = data.invoiceData.discountPercentage;
    // state.invoice.invoiceData.discountedAmount    = data.invoiceData.discountedAmount;
    // state.invoice.invoiceData.taxIncl             = data.invoiceData.taxIncl;
    // state.invoice.invoiceData.taxPercent          = data.invoiceData.taxPercent;
    // state.invoice.invoiceData.tax                 = data.invoiceData.tax;
    // state.invoice.invoiceData.grandTotal          = data.invoiceData.grandTotal;
    state.invoice = data;
}

}

export default mutations

