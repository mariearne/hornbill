## About Hornbill

https://hornbill.io

Hornbill is a free, clean, minimal and elegant invoicing web application for freelancers, startups and entrepreneurs. It is developed using Vuejs and deployed
using dockers on my VPS (Virtual Private Server).

## TODOS

- [x] Fix Cover Photo
- [x] Implement pdf view
- [x] Develop a backend for Hornbill
- [ ] Design more invoice templates
- [ ] And many more

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
