# build environment
# FROM node:12.2.0-alpine as build
# WORKDIR /app
# ENV PATH /app/node_modules/.bin:$PATH
# COPY package.json /app/package.json
# RUN yarn
# RUN yarn add @vue/cli@3.7.0 -g
# COPY . /app
# RUN yarn build

# production environment
FROM nginx:1.16.0-alpine
# COPY --from=build /app/dist /usr/share/nginx/html
COPY ./dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
